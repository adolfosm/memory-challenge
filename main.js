
var score = 0, 
    userName = "", 
    address = "", 
    elementsRemoved = [],
    highscores = [],
    pair = [],
    k = 0,
    j = 0;
    arr = [[], [], [], []],
    selected = arr[k][j];
     
const localScore = document.querySelector("#score-text");
const cardsFound = document.querySelector(".cards-found");

( function game() {
    shuffle();
    setNavigationArray();
    document.addEventListener("keydown", ((e) => {
        switch (e.keyCode) {
            case 37:
                if (j>0) {
                    j--;
                    setSelected();
                }
                break;
            case 38:
                if (k>0) {
                    k--;
                    setSelected();
                }                
                break;
            case 39:
                if (j<3) {
                    j++;
                    setSelected();
                }
                break;
            case 40:
                if (k<3) {
                    k++;
                    setSelected();
                }
                break;
            case 13:
                if (isRemoved(selected) || pair.length > 1) {
                    
                    return ;
                }

                const card = selected.querySelector(".back-card");
                
                if (pair.length == 0) {
                    card.classList.contains("hide") ? card.classList.remove("hide") : card.classList.add("hide");
                    pair.push(selected);                    
                    return;
                }

                if (pair[0].id !== selected.id) {
                    card.classList.contains("hide") ? card.classList.remove("hide") : card.classList.add("hide");
                    pair.push(selected);
                    setTimeout( () => { pair =  isMatch(pair)}, 850);
                }
                break;   
            default:
                break;
        }
    }))
})();

//Checks if there is a match pair and removes it
function isMatch(array) {
    if(array.length !== 2 ) {
        return array;
    } 
    const card1 = array[0].querySelector(".front-card");;
    const card2 = array[1].querySelector(".front-card");;
    if (card1.alt == card2.alt) {
        score ++;
        card1.classList.add("hide")
        card2.classList.add("hide")
        elementsRemoved.push(array[0]);
        elementsRemoved.push(array[1]);
        cardsFound.innerText = elementsRemoved.length;
        localScore.innerText = score;
        if (elementsRemoved.length === 16) {
            setTimeout(() => {
                const userData = getUserData();
                postData(userData);
                fetchHighscores();
            }, 500);
        }
    } else {
        array[0].querySelector(".back-card").classList.remove("hide");
        array[1].querySelector(".back-card").classList.remove("hide");
        score > 0 ? score--: score;
        localScore.innerText = score;
    }
    return [];
}
//Verifies if an element is already removed
function isRemoved(element){
    for (let i = 0; i < elementsRemoved.length; i++) {
        if(element === elementsRemoved[i]){
            return true
        }
    }
    return false;
}

function shuffle() {
    let gameboard = document.querySelector(".gameboard");
    let cards = [];
    // Adds cards to the array and removes them from the parent element
    document.querySelectorAll(".card").forEach(element => {
        cards.push(element);
        gameboard.removeChild(element);
    });
    
    // Shuffles the cards of the array
    for (let i = 0; i < cards.length - 1; i++) {
        const position = Math.floor (Math.random() *   (i + 1));
        const temp = cards[i];
        cards[i] = cards[position];
        cards[position] = temp;
    }
    // Appends the new cards to the parent element
    for (let i = 0; i < cards.length; i++) {
        const element = cards[i];
        gameboard.appendChild(element);
    }
    return gameboard;
};

function getUserData() {
    alert("Game Over");
    const namePrompt = prompt("Please enter your name:", "" );
    const addressPrompt = prompt("Please enter your email address:", "" );

    return {
      userName: namePrompt == null || namePrompt == "" ? "N/A" : namePrompt,
      address: addressPrompt == null || addressPrompt == "" ? "N/A" : addressPrompt
    };
}

function fetchHighscores() {
    const score = [];
    for (const key in localStorage) {
        if (Object.hasOwnProperty.call(localStorage, key)) {
            const element = localStorage[key];
            highscores.push(JSON.parse(element));
        }
    }
    highscores.sort((a, b) => b.score - a.score);
    highscores.forEach(element => {
        score.push(`Name: ${element.name}  || Address: ${element.address} || Score: ${element.score} \n`);
    });
    alert(`Scoreboard: 
    ${score.toString()}`);
}

function postData() {
    userName !== undefined && address !== undefined && score !== undefined ? localStorage.setItem(`${userName}${score}`, 
    JSON.stringify({ 
        'name': userName, 
        'address': address, 
        'score': score})): console.log("Undefined data not sent");
}

function setNavigationArray() {
    const cards = document.querySelectorAll(".card");
    for (let i = 0; i < cards.length; i++) {
        const element = cards[i];
        if (i<4) {
            arr[0].push(element);
        } else if(i<8) {
            arr[1].push(element);
        } else if(i<12){
            arr[2].push(element);
        } else if (i<16) {
            arr[3].push(element);
        }
    }
    selected = arr[k][j];
    selected.classList.add("selected");
}

function setSelected() {
    selected.classList.remove("selected");
    selected = arr[k][j];
    selected.classList.add("selected");
}

function resetGame() {
    shuffle();
    const cards = document.querySelectorAll(".card")
    cards.forEach(card => {
        if (card.classList.contains("selected")) {
            card.classList.remove("selected")
        }
        card.querySelector(".front-card").classList.remove("hide");
        card.querySelector(".back-card").classList.remove("hide");
    })
    score = 0; 
    localScore.innerText = score;
    cardsFound.innerText = 0;
    userName = ""; 
    address = "";
    elementsRemoved = [];
    pair = [];
    k = 0;
    j = 0;
    arr = [[], [], [], []];
    setNavigationArray();
}

